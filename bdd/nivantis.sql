-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 30 juin 2019 à 18:28
-- Version du serveur :  10.1.38-MariaDB
-- Version de PHP :  7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `nivantis`
--

-- --------------------------------------------------------

--
-- Structure de la table `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migration_versions`
--

INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
('20190618174247', '2019-06-18 17:43:28'),
('20190619180955', '2019-06-19 18:10:19'),
('20190619185049', '2019-06-19 18:51:04'),
('20190629163651', '2019-06-29 16:37:06'),
('20190630154316', '2019-06-30 15:44:13'),
('20190630160210', '2019-06-30 16:02:27');

-- --------------------------------------------------------

--
-- Structure de la table `officines_questions`
--

CREATE TABLE `officines_questions` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_salarie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `moy_client` double NOT NULL,
  `chiffre_affaire` double NOT NULL,
  `prod_plus_vendu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prod_moins_vendu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_redacteur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom_redacteur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_jour` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `officines_questions`
--

INSERT INTO `officines_questions` (`id`, `nom`, `adresse`, `nb_salarie`, `moy_client`, `chiffre_affaire`, `prod_plus_vendu`, `prod_moins_vendu`, `ville`, `code_postal`, `nom_redacteur`, `prenom_redacteur`, `date_jour`) VALUES
(4, 'Pharmacie Auteuil', '41 rue Victor Hugo', '15', 60, 200000, 'Dolipranes', 'Crèmes', 'Paris', '75017', 'Dupont', 'Charles', '2019-06-30');

-- --------------------------------------------------------

--
-- Structure de la table `pharmacie`
--

CREATE TABLE `pharmacie` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `achat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vente` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `formation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nb_salarie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jour_ouverture` int(11) NOT NULL,
  `horaire_ouverture` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `pharmacie`
--

INSERT INTO `pharmacie` (`id`, `nom`, `adresse`, `achat`, `vente`, `formation`, `nb_salarie`, `jour_ouverture`, `horaire_ouverture`, `ville`, `code_postal`) VALUES
(4, 'Pharmacie', '38 rue Fessart', '150', '120', 'Oui', '8', 6, '8', 'Boulogne-Billancourt', 92100);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`) VALUES
(3, 'admin@admin.fr', 'admin', '$2y$13$lKDuX.gAoKZ5SvsHB4Y/HO9Y0kJ8qL3Q.5M7oYB4IQKFdBtbNzEZe');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `officines_questions`
--
ALTER TABLE `officines_questions`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `pharmacie`
--
ALTER TABLE `pharmacie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `officines_questions`
--
ALTER TABLE `officines_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `pharmacie`
--
ALTER TABLE `pharmacie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
