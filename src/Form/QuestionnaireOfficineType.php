<?php

namespace App\Form;

use App\Entity\OfficinesQuestions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionnaireOfficineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomRedacteur')
            ->add('prenomRedacteur')
            ->add('dateJour')
            ->add('nom')
            ->add('adresse')
            ->add('ville')
            ->add('codePostal')
            ->add('nbSalarie')
            ->add('moyClient')
            ->add('chiffreAffaire')
            ->add('prodPlusVendu')
            ->add('prodMoinsVendu')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OfficinesQuestions::class,
        ]);
    }
}
