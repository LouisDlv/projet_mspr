<?php

namespace App\Controller;

use App\Entity\OfficinesQuestions;
use App\Entity\Pharmacie;
use App\Form\PharmacieFormType;
use App\Form\QuestionnaireOfficineType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Dompdf\Dompdf;
use Dompdf\Options;

class NivantisController extends AbstractController
{
    /**
     * @Route("/menu_nivantis", name="menu_nivantis")
     */
    public function menuNivantis()
    {
        return $this->render('nivantis/menu_nivantis.html.twig', [
            'controller_name' => 'NivantisController',
        ]);
    }

    /**
     * @Route("/menu_nivantis/pharmacie_nivantis", name="pharmacie_nivantis")
     */
    public function PharmacieNivantis()
    {
        return $this->render('nivantis/pharmacie_nivantis.html.twig', [
            'controller_name' => 'NivantisController',
        ]);
    }

    /**
     * @Route("/menu_nivantis/pharmacie_add", name="pharmacie_add")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function AddPharmacie(Request $request)
    {
        $pharmacie = new Pharmacie();

        $form = $this->createForm(PharmacieFormType::class, $pharmacie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pharmacie);
            $em->flush();
        }

        return $this->render('nivantis/pharmacie_add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/menu_nivantis/pharmacie_nivantis/list_pharmacie", name="pharmacie_list")
     */
    public function PharamacieList()
    {
        $repository = $this->getDoctrine()->getRepository('App:Pharmacie');

        $pharmacies = $repository->findAll();

        return $this->render('nivantis/pharmacieList.html.twig', array('pharmacies' => $pharmacies));
    }

    /**
     * @param Pharmacie $pharmacie
     * @param Request $request
     * @return Response
     * @Route("/menu_nivantis/pharmacie_nivantis/list_pharmacie/edit/{id}", name="pharmacie_edit")
     */
    public function editPharmacie(Pharmacie $pharmacie, Request $request)
    {
        $form = $this->createForm(PharmacieFormType::class, $pharmacie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->render('nivantis/pharmacie_add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/menu_nivantis/list_officine", name="officine_list")
     */
    public function OfficineList()
    {
        $repository = $this->getDoctrine()->getRepository('App:OfficinesQuestions');

        $officines = $repository->findAll();

        return $this->render('nivantis/officineList.html.twig', array('officines' => $officines));
    }

    /**
     * * @Route("/menu_nivantis/list_officine/edit/{id}", name="officine_pdf")
     * @param OfficinesQuestions $officine
     * @param Request $request
     * @return Response
     */
    public function pdfOfficine(OfficinesQuestions $officine, Request $request)
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);
        $form = $this->createForm(QuestionnaireOfficineType::class, $officine);

        $form->handleRequest($request);

        // Retrieve the HTML generated in our twig file
        $html = $this->render('nivantis/officines_pdf.html.twig', [
            'form' => $form->createView()
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("mypdf.pdf", [
            "Attachment" => true
        ]);
    }

}
