<?php

namespace App\Controller;

use App\Entity\Calcul;
use App\Entity\OfficinesQuestions;
use App\Entity\Pharmacie;
use App\Form\PharmacieFormType;
use App\Form\QuestionnaireOfficineType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DmoController extends AbstractController
{
    /**
     * @Route("menu_dmo", name="menu_dmo")
     */
    public function menuDmo()
    {
        return $this->render('dmo/menu_dmo.html.twig', [
            'controller_name' => 'DmoController',
        ]);
    }

    /**
     * @Route("menu_dmo/calculatrice", name="calculatrice")
     */
    public function calculatrice()
    {
        return $this->render('dmo/calculatrice.html.twig', [
            'controller_name' => 'DmoController',
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("menu_dmo/officines", name="officines")
     */
    public function officines(Request $request)
    {
        $officines = new OfficinesQuestions();

        $form = $this->createForm(QuestionnaireOfficineType::class, $officines);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($officines);
            $em->flush();
        }

        return $this->render('dmo/officines.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("menu_dmo/find_pharmacie", name="find_pharmacie")
     */
    public function findPharmacie ()
    {
        return $this->render('dmo/find_pharmacie.html.twig', [
            'controller_name' => 'DmoController',
        ]);
    }

    /**
     * @Route("menu_dmo/geo_pharmacie", name="geo_pharmacie")
     */
    public function geoPharmacie ()
    {
        return $this->render('dmo/geo_pharmacie.html.twig', [
            'controller_name' => 'DmoController',
        ]);
    }

    /**
     * @Route("/menu_dmo/find_pharmacie/list_pharmacie", name="pharmacie_list_dmo")
     */
    public function PharamacieList()
    {
        $repository = $this->getDoctrine()->getRepository('App:Pharmacie');

        $pharmacies = $repository->findAll();

        return $this->render('dmo/pharmacieList.html.twig', array('pharmacies' => $pharmacies));
    }

    /**
     * @param Pharmacie $pharmacie
     * @param Request $request
     * @return Response
     * @Route("/menu_dmo/find_pharmacie/list_pharmacie/see/{id}", name="pharmacie_see")
     */
    public function seePharmacie(Pharmacie $pharmacie, Request $request)
    {
        $form = $this->createForm(PharmacieFormType::class, $pharmacie);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();
        }

        return $this->render('dmo/pharmacie_see.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("menu_dmo/calculatrice/prix_achat", name="prix_achat")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function prixAchat (Request $request)
    {
        $task = new Calcul();

        $form = $this->createFormBuilder($task)
            ->add('achatBrut', TextType::class, [
                'attr' => [
                    'placeholder' => 'Achat Brut',
                    'class' => 'form-control'
                ]
            ])
            ->add('tauxRemise', TextType::class, [
                'attr' => [
                    'placeholder' => 'Taux de remise',
                    'class' => 'form-control'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $result = $task->OperationPrixAchat();
            return $this->render('dmo/calculatrice/prix_achat.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result
                )
            );
        }
        return $this->render('dmo/calculatrice/prix_achat.html.twig', array('form' => $form->createView()));
    }


    /**
     * @Route("menu_dmo/calculatrice/taux_remise", name="taux_remise")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function tauxRemise (Request $request)
    {
        $task = new Calcul();

        $form = $this->createFormBuilder($task)
            ->add('achatBrut', TextType::class, [
                'attr' => [
                    'placeholder' => 'Achat Brut',
                    'class' => 'form-control'
                ]
            ])
            ->add('achatNet', TextType::class, [
                'attr' => [
                    'placeholder' => 'Achat Net',
                    'class' => 'form-control'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $result = $task->OperationTauxRemise();
            return $this->render('dmo/calculatrice/taux_remise.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result
                )
            );
        }
        return $this->render('dmo/calculatrice/taux_remise.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("menu_dmo/calculatrice/prix_vente", name="prix_vente")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function prixVente(Request $request)
    {
        $task = new Calcul();

        $form = $this->createFormBuilder($task)
            ->add('achatNet', TextType::class, [
                'attr' => [
                    'placeholder' => 'Achat Net',
                    'class' => 'form-control'
                ]
            ])
            ->add('coefficient', TextType::class, [
                'attr' => [
                    'placeholder' => 'Coefficient',
                    'class' => 'form-control'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $result = $task->OperationVenteNet();
            return $this->render('dmo/calculatrice/prix_vente.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result
                )
            );
        }
        return $this->render('dmo/calculatrice/prix_vente.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("menu_dmo/calculatrice/coefficient", name="coefficient")
     */
    public function coefficient(Request $request)
    {
        $task = new Calcul();

        $form = $this->createFormBuilder($task)
            ->add('venteNet', TextType::class, [
                'attr' => [
                    'placeholder' => 'Vente Net',
                    'class' => 'form-control'
                ]
            ])
            ->add('achatNet', TextType::class, [
                'attr' => [
                    'placeholder' => 'Achat Net',
                    'class' => 'form-control'
                ]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $result = $task->OperationCoefficient();
            return $this->render('dmo/calculatrice/coefficient.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result
                )
            );
        }
        return $this->render('dmo/calculatrice/coefficient.html.twig', array('form' => $form->createView()));
    }
}
