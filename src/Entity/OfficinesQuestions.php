<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OfficinesQuestionsRepository")
 */
class OfficinesQuestions
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string")
     */
    private $nbSalarie;

    /**
     * @ORM\Column(type="float")
     */
    private $moyClient;

    /**
     * @ORM\Column(type="float")
     */
    private $chiffreAffaire;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prodPlusVendu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prodMoinsVendu;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $codePostal;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomRedacteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prenomRedacteur;

    /**
     * @ORM\Column(type="string")
     */
    private $dateJour;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getNbSalarie(): ?string
    {
        return $this->nbSalarie;
    }

    public function setNbSalarie(string $nbSalarie): self
    {
        $this->nbSalarie = $nbSalarie;

        return $this;
    }

    public function getMoyClient(): ?float
    {
        return $this->moyClient;
    }

    public function setMoyClient(float $moyClient): self
    {
        $this->moyClient = $moyClient;

        return $this;
    }

    public function getChiffreAffaire(): ?float
    {
        return $this->chiffreAffaire;
    }

    public function setChiffreAffaire(float $chiffreAffaire): self
    {
        $this->chiffreAffaire = $chiffreAffaire;

        return $this;
    }

    public function getProdPlusVendu(): ?string
    {
        return $this->prodPlusVendu;
    }

    public function setProdPlusVendu(string $prodPlusVendu): self
    {
        $this->prodPlusVendu = $prodPlusVendu;

        return $this;
    }

    public function getProdMoinsVendu(): ?string
    {
        return $this->prodMoinsVendu;
    }

    public function setProdMoinsVendu(string $prodMoinsVendu): self
    {
        $this->prodMoinsVendu = $prodMoinsVendu;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getNomRedacteur(): ?string
    {
        return $this->nomRedacteur;
    }

    public function setNomRedacteur(string $nomRedacteur): self
    {
        $this->nomRedacteur = $nomRedacteur;

        return $this;
    }

    public function getPrenomRedacteur(): ?string
    {
        return $this->prenomRedacteur;
    }

    public function setPrenomRedacteur(string $prenomRedacteur): self
    {
        $this->prenomRedacteur = $prenomRedacteur;

        return $this;
    }

    public function getDateJour(): ?string
    {
        return $this->dateJour;
    }

    public function setDateJour(string $dateJour): self
    {
        $this->dateJour = $dateJour;

        return $this;
    }
}
