<?php
/**
 * Created by PhpStorm.
 * User: Alexis Motro
 * Date: 21/05/2019
 * Time: 11:55
 */

namespace App\Entity\Operations;


use App\Entity\OperationInterface;

class CalculGlobal implements OperationInterface
{
    public function PrixAchat($achatBrut, $tauxRemise)
    {
        return $achatBrut * (1 - $tauxRemise) ;
    }

    public function TauxRemise($achatBrut, $achatNet)
    {
        return (1 - $achatNet / $achatBrut) * 100;
    }

    public function VenteNet($coefficient, $achatNet)
    {
        return $achatNet * $coefficient;
    }

    public function Coefficient($venteNet, $achatNet)
    {
        return $venteNet / $achatNet;
    }
}