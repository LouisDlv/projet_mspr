<?php
/**
 * Created by PhpStorm.
 * User: Alexis Motro
 * Date: 21/05/2019
 * Time: 10:16
 */

namespace App\Entity;


use App\Entity\Operations\CalculGlobal;

class Calcul
{
    public $achatBrut;
    public $tauxRemise;
    public $achatNet;
    public $coefficient;
    public $venteNet;

    public function getAchatBrut()
    {
        return $this->achatBrut;
    }

    public function setAchatBrut($achatBrut)
    {
        $this->achatBrut = $achatBrut;
    }

    public function getAchatNet()
    {
        return $this->achatNet;
    }

    public function setAchatNet($achatNet)
    {
        $this->achatNet = $achatNet;
    }

    public function getVenteNet()
    {
        return $this->venteNet;
    }

    public function setVenteNet($venteNet)
    {
        $this->venteNet = $venteNet;
    }

    public function getCoefficient()
    {
        return $this->coefficient;
    }

    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;
    }

    public function getTauxRemise()
    {
        return $this->tauxRemise;
    }

    public function setTauxRemise($tauxRemise)
    {
        $this->tauxRemise = $tauxRemise;
    }

    public function OperationPrixAchat()
    {
        $operation = new CalculGlobal();
        return $operation->PrixAchat($this->getAchatBrut(), $this->getTauxRemise());
    }

    public function OperationTauxRemise()
    {
        $operation = new CalculGlobal();
        return $operation->TauxRemise($this->getAchatBrut(), $this->getAchatNet());
    }

    public function OperationVenteNet()
    {
        $operation = new CalculGlobal();
        return $operation->VenteNet($this->getCoefficient(), $this->getAchatNet());
    }

    public function OperationCoefficient()
    {
        $operation = new CalculGlobal();
        return $operation->Coefficient($this->getAchatNet(), $this->getVenteNet());
    }
}