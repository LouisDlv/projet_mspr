<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PharmacieRepository")
 */
class Pharmacie
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $achat;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $vente;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $formation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nbSalarie;

    /**
     * @ORM\Column(type="integer")
     */
    private $jourOuverture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $horaireOuverture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ville;

    /**
     * @ORM\Column(type="integer")
     */
    private $codePostal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getAchat(): ?string
    {
        return $this->achat;
    }

    public function setAchat(string $achat): self
    {
        $this->achat = $achat;

        return $this;
    }

    public function getVente(): ?string
    {
        return $this->vente;
    }

    public function setVente(string $vente): self
    {
        $this->vente = $vente;

        return $this;
    }

    public function getFormation(): ?string
    {
        return $this->formation;
    }

    public function setFormation(string $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getNbSalarie(): ?string
    {
        return $this->nbSalarie;
    }

    public function setNbSalarie(string $nbSalarie): self
    {
        $this->nbSalarie = $nbSalarie;

        return $this;
    }

    public function getJourOuverture(): ?int
    {
        return $this->jourOuverture;
    }

    public function setJourOuverture(int $jourOuverture): self
    {
        $this->jourOuverture = $jourOuverture;

        return $this;
    }

    public function getHoraireOuverture(): ?string
    {
        return $this->horaireOuverture;
    }

    public function setHoraireOuverture(string $horaireOuverture): self
    {
        $this->horaireOuverture = $horaireOuverture;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCodePostal(): ?int
    {
        return $this->codePostal;
    }

    public function setCodePostal(int $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }
}
