<?php
/**
 * Created by PhpStorm.
 * User: Alexis Motro
 * Date: 21/05/2019
 * Time: 11:33
 */

namespace App\Entity;


interface OperationInterface
{
    public function PrixAchat($tauxRemise, $achatBrut);
    public function TauxRemise($achatNet, $achatBrut);
    public function VenteNet($achatNet, $coefficient);
    public function Coefficient($venteNet, $achatNet);
}